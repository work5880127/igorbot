"""User table finally

Revision ID: 202521b8e726
Revises: c77b2fd36ccb
Create Date: 2021-09-22 17:20:16.557177

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '202521b8e726'
down_revision = 'c77b2fd36ccb'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('personal_chat_id', sa.Integer),
        sa.Column('telegram_id', sa.Integer),
        sa.Column('command_seq', sa.String, default='{}'),
        sa.Column('banned', sa.Boolean, default=False)
    )


def downgrade():
    op.drop_table('user')
