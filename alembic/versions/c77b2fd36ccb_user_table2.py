"""User table2

Revision ID: c77b2fd36ccb
Revises: 2a17309fe1fb
Create Date: 2021-09-22 17:15:35.550741

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c77b2fd36ccb'
down_revision = '2a17309fe1fb'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
