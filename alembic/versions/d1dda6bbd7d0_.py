"""empty message

Revision ID: d1dda6bbd7d0
Revises: efe0620445c6
Create Date: 2022-07-10 16:14:03.117610

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd1dda6bbd7d0'
down_revision = 'efe0620445c6'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("user", sa.Column("is_admin", sa.Boolean, default=False))


def downgrade():
    op.drop_column("user", "is_admin")
