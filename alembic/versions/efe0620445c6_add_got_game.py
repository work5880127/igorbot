"""add got game

Revision ID: efe0620445c6
Revises: 202521b8e726
Create Date: 2021-10-28 20:59:29.158673

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'efe0620445c6'
down_revision = '202521b8e726'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("user", sa.Column("playing_got_game", sa.Boolean, default=False))
    op.add_column("user", sa.Column("name", sa.String))


def downgrade():
    op.drop_column("user", "playing_got_game")
    op.drop_column("user", "name")
