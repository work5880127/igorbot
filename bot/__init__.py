import telebot
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from config import token, log_file
import logging


bot = telebot.TeleBot(token)
engine = create_engine("sqlite:///bot.db")
Session = sessionmaker(bind=engine)
logging.basicConfig(filename=log_file, level='INFO')


from bot import actions, filters, models, functions
