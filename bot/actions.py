from telebot.types import Message
from telebot.types import ReplyKeyboardRemove as remove_kb
from bot.filters import is_admin, check_command_seq
from bot.constants import TIC_TAK_TOE_NULL, TIC_TAK_TOE_FIRST, TIC_TAK_TOE_SECOND
from bot import bot, Session
from bot.models import User
from bot.keyboards import select_game_kb, go_kb, tic_tak_toe_kb, got_game_kb, got_chose_mode_kb
from bot.functions import get_user, tic_tak_toe_win, tic_tak_toe_bot, got_metric
from itertools import permutations
import json
import re
import logging


got_games = {}  # TODO: to dump into db


@bot.message_handler(commands=["start"])
def welcome(message: Message):
    logging.info(f'{message.chat.id} used /start')
    session = Session()
    if session.query(User).filter(User.telegram_id == message.from_user.id).one_or_none() is None:
        user = User(telegram_id=message.from_user.id,
                    personal_chat_id=message.sender_chat or message.chat.id,
                    name=message.from_user.first_name)
        session.add(user)
    else:
        user = session.query(User).filter(User.telegram_id == message.from_user.id).one()
        user.telegram_id = message.from_user.id
        user.personal_chat_id = message.sender_chat or message.chat.id
        user.name = message.from_user.first_name

    session.commit()
    bot.send_message(message.chat.id, open('bot/messages/welcome.txt', 'r', encoding='utf-8').read())


@bot.message_handler(commands='end')
def end(message: Message):
    session, user = get_user(message)
    if not user.is_admin:
        logging.warning(f'{user.telegram_id} (not admin) used /end to clear {user.command_seq}')
    user.command_seq = '{}'
    res = True
    for key in got_games.keys():
        if user.id in got_games[key].keys():
            res = False
    if res:
        user.playing_got_game = False
    bot.send_message(message.from_user.id, 'Ok...', reply_markup=remove_kb())
    session.commit()


@bot.message_handler(commands=["help"])
def help_page(message: Message):
    bot.send_message(message.chat.id, open('bot/messages/help.txt', encoding='utf-8').read())


@bot.message_handler(regexp=r"нет")
def no_you(message: Message):
    bot.reply_to(message, "Сам нет")


@bot.message_handler(func=is_admin, commands=["spam"])
def spam(message: Message):
    try:
        command, chat, num = message.text.split()
        num, chat = int(num), int(chat)
    except ValueError:
        bot.send_message(message.from_user.id, open('bot/messages/spam_doc.txt', encoding='utf-8').read())
        return
    bot.send_message(message.from_user.id, "Select the sticker")
    session, admin = get_user(message)
    admin.command_seq = json.dumps({'spam': {'num': num, 'chat': chat}}, separators=(',', ':'))
    session.commit()


@bot.message_handler(func=check_command_seq(['spam']), content_types=['sticker'])
def get_sticker_for_spam(message: Message):
    session, user = get_user(message)
    val = json.loads(user.command_seq)['spam']
    user.command_seq = '{}'
    session.commit()
    sticker_id = message.sticker.file_id
    try:
        for i in range(val['num']):
            bot.send_sticker(val['chat'], sticker_id)
    except TypeError:
        bot.send_message(message.from_user.id, open("bot/messages/spam_doc.txt", 'r', encoding='utf8').read())


# GOTGame
@bot.message_handler(commands=['start_got_game'])
def start_got_game(message: Message):
    session, user = get_user(message)
    password = message.text.split()
    if len(password) != 2 or user.playing_got_game:
        bot.send_message(message.from_user.id, open('bot/messages/got_game_doc.txt', 'r', encoding='utf8').read())
        return
    _, password = password
    if password in got_games.keys():
        bot.send_message(message.from_user.id, "This id is already used")
        return
    got_games[password] = {"owner": user.id}
    user.playing_got_game = True
    bot.send_message(message.from_user.id, f"Successfully started game with id: {password}\nEnter by pressing Go", reply_markup=go_kb)
    user.command_seq = json.dumps({'enter_got_game': {'password': password}})
    session.commit()


@bot.message_handler(commands=['enter_got_game'])
@bot.message_handler(func=check_command_seq(['enter_got_game']))
def enter_got_game(message: Message):
    session, user = get_user(message)
    seq = message.text.split()
    if len(seq) != 2:
        if check_command_seq(['enter_got_game'])(message):
            password = json.loads(user.command_seq)['enter_got_game']['password']
        else:
            bot.send_message(message.from_user.id, open('bot/messages/got_game_doc.txt', 'r', encoding='utf8').read())
            return
    else:
        _, password = seq
    if password not in got_games.keys():
        bot.send_message(message.from_user.id, "No game with this id")
        return
    if len(got_games[password]) == 8:
        bot.send_message(message.from_user.id, "The game is already full")
        return
    bot.send_message(message.from_user.id, "Enter your favourite fraction", reply_markup=got_game_kb([]))
    user.command_seq = json.dumps({'enter_got_game': {'password': password}, 'select_fraction': {}})
    session.commit()


@bot.message_handler(func=check_command_seq(['enter_got_game', 'select_fraction']))
def select_fraction(message: Message):
    session, user = get_user(message)
    nation_num = {"Stark": 0,
                  "Tyrell": 1,
                  "Grayjoy": 2,
                  "Lannister": 3,
                  "Baratheon": 4,
                  "Arren": 5,
                  "Martell": 6}
    command_seq = json.loads(user.command_seq)
    if message.text not in nation_num.keys() or \
            any([nation_num[message.text] in command_seq['select_fraction'][key]
                 for key in command_seq['select_fraction'].keys()]):
        pass
    elif len(command_seq['select_fraction']) == 0:
        command_seq['select_fraction']['loved'] = [nation_num[message.text]]
        bot.send_message(message.from_user.id, "Enter your second favourite nation",
                         reply_markup=got_game_kb(command_seq['select_fraction']['loved']))
    elif len(command_seq['select_fraction']['loved']) == 1:
        command_seq['select_fraction']['loved'].append(nation_num[message.text])
        bot.send_message(message.from_user.id, "Enter the nation you're ok with",
                         reply_markup=got_game_kb(command_seq['select_fraction']['loved']))
    elif len(command_seq['select_fraction']['loved']) == 2:
        command_seq['select_fraction']['loved'].append(nation_num[message.text])
        bot.send_message(message.from_user.id, "Enter the nation you'll hate to play",
                         reply_markup=got_game_kb(command_seq['select_fraction']['loved']))
    elif len(command_seq['select_fraction']) == 1:
        command_seq['select_fraction']['hated'] = [nation_num[message.text]]
        bot.send_message(message.from_user.id, "One more",
                         reply_markup=got_game_kb(command_seq['select_fraction']['loved'] + [nation_num[message.text]]))
    elif len(command_seq['select_fraction']) == 2:
        command_seq['select_fraction']['hated'].append(nation_num[message.text])
        bot.send_message(message.from_user.id, open('bot/messages/got_game_status_disc.txt', encoding='utf-8').read(),
                         reply_markup=got_chose_mode_kb)
    if message.text not in ['Normal', 'Whatever', 'Determined']:
        pass
    elif len(command_seq['select_fraction']) == 2 and len(command_seq['select_fraction']['hated']) == 2:
        command_seq['select_fraction']['mode'] = message.text
        nation = ["Stark", "Tyrell", "Grayjoy", "Lannister", "Baratheon", "Arren", "Martell"]
        bot.send_message(message.from_user.id,
                         open('bot/messages/got_game_setup.txt', encoding='utf-8').read().format(
                             nation[command_seq['select_fraction']['loved'][0]],
                             nation[command_seq['select_fraction']['loved'][1]],
                             nation[command_seq['select_fraction']['loved'][2]],
                             nation[command_seq['select_fraction']['hated'][0]],
                             nation[command_seq['select_fraction']['hated'][1]],
                             command_seq['select_fraction']['mode'],
                             command_seq['enter_got_game']['password']
                         ), reply_markup=remove_kb())
        owner = session.query(User).filter(User.id == got_games[command_seq['enter_got_game']['password']]['owner']).one()
        bot.send_message(owner.telegram_id, f"{user.name} has finished")
        got_games[command_seq['enter_got_game']['password']][user.id] = command_seq['select_fraction']
        command_seq = {}
    if message.text not in ['Normal', 'Whatever', 'Determined'] and message.text not in nation_num.keys():
        bot.send_message(message.from_user.id, "Please, use the keyboard options")
    user.command_seq = json.dumps(command_seq)
    session.commit()


@bot.message_handler(commands=['got_game_calc'])
def calc_got_game(message: Message):
    seq = message.text.split()
    if len(seq) != 2:
        bot.send_message(message.from_user.id, "Enter the id correctly")
        return
    _, password = seq
    if password not in got_games.keys():
        bot.send_message(message.from_user.id, "There is no game with this id")
        return
    session, user = get_user(message)
    if user.id != got_games[password]['owner']:
        bot.send_message(message.from_user.id, "You cannot use this command with the given game id")
        return

    got_games[password].pop('owner')
    strange_names = ['a' * (i + 1) for i in range(7)]
    for i in range(len(got_games[password]), 7):
        got_games[password][strange_names[i]] = {'loved': [1, 2, 3], 'hated': [1, 2], 'mode': 'bot'}
    s = list(got_games[password].values())
    best = 0
    res = []
    for per in permutations(range(7)):
        calc = sum(got_metric(s[i], per[i]) for i in range(7))
        if calc > best:
            res = per
            best = calc
    bot.send_message(message.from_user.id,
                     open('bot/messages/got_answer_template.txt', encoding='utf-8').read().format(best))
    owner = user
    nation = ["Stark", "Tyrell", "Grayjoy", "Lannister", "Baratheon", "Arren", "Martell"]
    names = list(got_games[password].keys())
    for i in range(7):
        player = names[i]
        if player in strange_names:
            continue
        user = session.query(User).filter(User.id == player).one()
        bot.send_message(owner.telegram_id, f"{user.name} will play {nation[res[i]]}")
        bot.send_message(user.telegram_id, f"You will play {nation[res[i]]}")
        user.playing_got_game = False
    got_games.pop(password)
    session.commit()


# games
@bot.message_handler(commands=['game'])
def select_game(message: Message):
    bot.send_message(message.from_user.id, 'Select the game', reply_markup=select_game_kb)
    session, user = get_user(message)
    user.command_seq = json.dumps({'game': {}}, separators=(',', ':'))
    session.commit()


@bot.message_handler(func=check_command_seq(['game']))
def game_selected(message: Message):
    keyboard = select_game_kb.keyboard
    games = [keyboard[i][j]['text'] for i in range(len(keyboard)) for j in range(len(keyboard[0]))]
    if message.text not in games:
        bot.send_message(message.from_user.id, open('bot/messages/game_doc.txt', 'r', encoding='utf8').read())
        return
    bot.send_message(message.from_user.id, 'Press Go to start', reply_markup=go_kb)
    session, user = get_user(message)
    user.command_seq = json.dumps({'game': {}, message.text: {}})
    session.commit()


# games/tic-tak-toe
@bot.message_handler(func=check_command_seq(['game', 'tic-tak-toe']))
def tic_tak_toe(message: Message):
    session, user = get_user(message)
    command_seq = json.loads(user.command_seq)
    converter = {
        'ㅤ' * 1: 'a1',
        'ㅤ' * 2: 'a2',
        'ㅤ' * 3: 'a3',
        'ㅤ' * 4: 'b1',
        'ㅤ' * 5: 'b2',
        'ㅤ' * 6: 'b3',
        'ㅤ' * 7: 'c1',
        'ㅤ' * 8: 'c2',
        'ㅤ' * 9: 'c3'
    }
    if re.fullmatch(r'ㅤ+', message.text) and len(message.text) <= 9:
        message.text = converter[message.text]
    if command_seq['tic-tak-toe'] == {}:
        command_seq['tic-tak-toe'] = {'a': {'1': TIC_TAK_TOE_NULL, '2': TIC_TAK_TOE_NULL, '3': TIC_TAK_TOE_NULL},
                                      'b': {'1': TIC_TAK_TOE_NULL, '2': TIC_TAK_TOE_NULL, '3': TIC_TAK_TOE_NULL},
                                      'c': {'1': TIC_TAK_TOE_NULL, '2': TIC_TAK_TOE_NULL, '3': TIC_TAK_TOE_NULL}
                                      }

    if TIC_TAK_TOE_FIRST == message.text or message.text == TIC_TAK_TOE_SECOND:
        bot.send_message(message.from_user.id, 'This space is already taken')
        user.command_seq = json.dumps(command_seq, separators=(',', ':'))
        session.commit()
        return

    if re.fullmatch(r'[abc][123]', message.text) is None:
        user.command_seq = json.dumps(command_seq, separators=(',', ':'))
        session.commit()
        bot.send_message(message.from_user.id, 'Your turn to choose',
                         reply_markup=tic_tak_toe_kb(command_seq['tic-tak-toe']))
        return

    row, column = message.text[0], message.text[1]
    if command_seq['tic-tak-toe'][row][column] != TIC_TAK_TOE_NULL:
        bot.send_message(message.from_user.id, 'This space is already taken')
        user.command_seq = json.dumps(command_seq, separators=(',', ':'))
        session.commit()
        return
    command_seq['tic-tak-toe'][row][column] = TIC_TAK_TOE_FIRST

    if tic_tak_toe_win(command_seq['tic-tak-toe']):
        bot.send_message(message.from_user.id, 'You win!', reply_markup=remove_kb())
        user.command_seq = '{}'
        session.commit()
        return
    rows = list(command_seq['tic-tak-toe'].keys())
    columns = list(command_seq['tic-tak-toe'][rows[0]].keys())
    vacant = [(i, j) for i in rows for j in columns if command_seq['tic-tak-toe'][i][j] == TIC_TAK_TOE_NULL]
    if len(vacant) == 0:
        bot.send_message(message.from_user.id, 'Stalemate...', reply_markup=remove_kb())
        user.command_seq = '{}'
        session.commit()
        return
    bot.send_chat_action(message.from_user.id, 'typing')
    row, column = tic_tak_toe_bot(command_seq['tic-tak-toe'])
    command_seq['tic-tak-toe'][row][column] = TIC_TAK_TOE_SECOND
    if tic_tak_toe_win(command_seq['tic-tak-toe']):
        bot.send_message(message.from_user.id, 'You lose :(', reply_markup=remove_kb())
        user.command_seq = '{}'
        session.commit()
        return
    bot.send_message(message.from_user.id, row + column,
                     reply_markup=tic_tak_toe_kb(command_seq['tic-tak-toe']))
    user.command_seq = json.dumps(command_seq, separators=(',', ':'))
    session.commit()
