import telebot
import json
from bot.functions import get_user
from bot import Session
from bot.models import User


def check_command_seq(seq):
    def check1(message):
        session = Session()
        commands = json.loads(session.query(User).filter(User.telegram_id == message.from_user.id).one().command_seq)
        session.close()
        commands = list(commands.keys())
        return commands == seq
    return check1


def is_admin(message):
    session, user = get_user(message)
    res = user.is_admin
    session.commit()
    return res and message.chat.type == "private"