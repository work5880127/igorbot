from telebot.types import Message
import random
from bot import Session
from bot.models import User
from bot.constants import TIC_TAK_TOE_NULL, TIC_TAK_TOE_FIRST, TIC_TAK_TOE_SECOND


def get_user(message: Message):
    session = Session()
    user = session.query(User).filter(User.telegram_id == message.from_user.id).one()
    return session, user


def tic_tak_toe_win(board: dict[str: dict[str: str, str: str, str: str],
                                str: dict[str: str, str: str, str: str],
                                str: dict[str: str, str: str, str: str]]):
    res = False
    rows = list(board.keys())
    columns = list(board[rows[0]].keys())
    for row in rows:
        if board[row][columns[0]] == board[row][columns[1]] == board[row][columns[2]] != TIC_TAK_TOE_NULL:
            res = True
    for column in columns:
        if board[rows[0]][column] == board[rows[1]][column] == board[rows[2]][column] != TIC_TAK_TOE_NULL:
            res = True
    if board[rows[0]][columns[0]] == board[rows[1]][columns[1]] == board[rows[2]][columns[2]] != TIC_TAK_TOE_NULL:
        res = True
    if board[rows[2]][columns[0]] == board[rows[1]][columns[1]] == board[rows[0]][columns[2]] != TIC_TAK_TOE_NULL:
        res = True
    return res


def tic_tak_toe_bot(board: dict[str: dict[str: str, str: str, str: str],
                                str: dict[str: str, str: str, str: str],
                                str: dict[str: str, str: str, str: str]]):
    rows = list(board.keys())
    columns = list(board[rows[0]].keys())

    # determine which player's turn it is
    cnt = {}
    for row in rows:
        for column in columns:
            cnt[board[row][column]] = cnt.get(board[row][column], 0) + 1
    cnt.pop(TIC_TAK_TOE_NULL)
    if len(cnt.items()) != 1 and (len(cnt.items()) == 0 or list(cnt.values())[0] == list(cnt.values())[1]):
        player = TIC_TAK_TOE_FIRST
        opponent = TIC_TAK_TOE_SECOND
    else:
        player = TIC_TAK_TOE_SECOND
        opponent = TIC_TAK_TOE_FIRST

    valid = [(i, j) for i in rows for j in columns if board[i][j] == TIC_TAK_TOE_NULL]
    # try to win
    for spot in valid:
        board[spot[0]][spot[1]] = player
        if tic_tak_toe_win(board):
            return spot
        board[spot[0]][spot[1]] = TIC_TAK_TOE_NULL
    # try to keep opponent from winning
    for spot in valid:
        board[spot[0]][spot[1]] = opponent
        if tic_tak_toe_win(board):
            return spot
        board[spot[0]][spot[1]] = TIC_TAK_TOE_NULL

    return random.choice(valid)


def got_metric(tier_list, pick):
    if tier_list['mode'] == "Normal":
        if pick == tier_list['loved'][0]:
            return 105
        if pick == tier_list['loved'][1]:
            return 104
        if pick == tier_list['loved'][2]:
            return 102
        if pick in tier_list['hated']:
            return -1000
    if tier_list['mode'] == 'Determined':
        if pick == tier_list['loved'][0]:
            return 105
        if pick == tier_list['loved'][1]:
            return 103
        if pick == tier_list['loved'][2]:
            return 102
        if pick in tier_list['hated']:
            return -1000
    if tier_list['mode'] == 'Whatever':
        if pick in tier_list['loved']:
            return 105
        if pick in tier_list['hated']:
            return -1000
    return 0
