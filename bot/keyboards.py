from telebot.types import ReplyKeyboardMarkup, KeyboardButton
from bot.constants import TIC_TAK_TOE_NULL

select_game_kb = ReplyKeyboardMarkup(one_time_keyboard=True)
b1 = KeyboardButton('tic-tak-toe')
select_game_kb.row(b1)


go_kb = ReplyKeyboardMarkup(one_time_keyboard=True)
b1 = KeyboardButton('Go!')
go_kb.row(b1)


def tic_tak_toe_kb(board: dict):
    tic_tak_toe_kb = ReplyKeyboardMarkup()
    rows = ['a', 'b', 'c']
    columns = ['1', '2', '3']
    for i in range(3):
        buttons = []
        for j in range(3):
            row = rows[i]
            column = columns[j]
            buttons.append(KeyboardButton('ㅤ' * (i * 3 + j + 1) if board[row][column] == TIC_TAK_TOE_NULL else board[row][column]))
        tic_tak_toe_kb.row(*buttons)
    return tic_tak_toe_kb


b1 = KeyboardButton("Normal")
b2 = KeyboardButton("Whatever")
b3 = KeyboardButton("Determined")
got_chose_mode_kb = ReplyKeyboardMarkup()
got_chose_mode_kb.row(b1)
got_chose_mode_kb.row(b2)
got_chose_mode_kb.row(b3)


def got_game_kb(used: list):
    got_game_kb = ReplyKeyboardMarkup()
    nations = ["Stark", "Tyrell", "Grayjoy", "Lannister", "Baratheon", "Arren", "Martell"]
    i = 0
    while i < len(nations):
        buttons = []
        while i < len(nations) and len(buttons) < 2:
            if i not in used:
                buttons.append(KeyboardButton(nations[i]))
            i += 1
        got_game_kb.row(*buttons)
    return got_game_kb
