from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Integer, String, Column, Boolean
Base = declarative_base()


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    telegram_id = Column(Integer)
    personal_chat_id = Column(Integer)
    name = Column(String)
    command_seq = Column(String, default='{}')
    banned = Column(Boolean, default=False)
    playing_got_game = Column(Boolean, default=False)
    is_admin = Column(Boolean, default=False)
